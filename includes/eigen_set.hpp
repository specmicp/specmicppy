/*-------------------------------------------------------

Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Princeton University nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

---------------------------------------------------------*/

#ifndef SPECMICP_CYTHON_EIGEN
#define SPECMICP_CYTHON_EIGEN

#include <Eigen/Core>

// Cython do not accept left-value references
// this is a workaround to set an value in a vector or a matrix

inline void vector_setitem(Eigen::VectorXd& vec, int row, double value) {
    vec(row) = value;
}

inline void vector_additem(Eigen::VectorXd& vec, int row, double value) {
    vec(row) += value;
}


inline void matrix_setitem(Eigen::MatrixXd& mat, int row, int col, double value) {
    mat(row, col) = value;
}

inline void matrix_additem(Eigen::MatrixXd& mat, int row, int col, double value) {
    mat(row, col) += value;
}

#endif // SPECMICP_CYTHON_EIGEN

