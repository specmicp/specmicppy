# definition from <memory>
#

from libcpp cimport bool

# TODO this has been added in cython 0.24 // remove it ? conditionnaly ?
cdef extern from "<memory>" namespace "std":
    #
    #   The shared ptr
    #       note : there is no overloading of operator->
    #       the true object is accessed through the get() member function
    #
    cdef cppclass shared_ptr[T]:
        shared_ptr()
        shared_ptr(shared_ptr[T])
        T* get()

    shared_ptr[T] make_shared[T]()

    cdef cppclass unique_ptr[T]:
        unique_ptr()
        unique_ptr(T*)
        unique_ptr(unique_ptr[T])
        T* get()
        void swap(unique_ptr[T])
        
        bool operator bool()
        bool operator==(nullptr_t)
        bool operator!=(nullptr_t)
        T* release()