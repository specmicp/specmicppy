#Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from libcpp.unordered_map cimport unordered_map as cumap
from libcpp cimport bool
from libcpp.string cimport string
from eigen cimport VectorXd
from memory cimport shared_ptr

from specmicp.database cimport DatabaseManager,DataContainer

from specmicp.solution cimport AdimensionalSystemSolution, SpecMiCPSolution
from specmicp.constraints cimport AdimensionalSystemConstraints, SpecMiCPConstraints
from specmicp.solver_options cimport MiCPPerformance, AdimensionalSystemSolverOptions, AdimensionalSystemOptions, MiCPSolverOptions, is_solver_successful

from specmicp.units cimport UnitsSet, UnitsBox

# The SpecMiCP Solver
# -------------------
cdef extern from "specmicp/adimensional/adimensional_system_solver.hpp" namespace "specmicp":
    cdef cppclass AdimensionalSystemSolver:
        AdimensionalSystemSolver(
            shared_ptr[DataContainer],
            const AdimensionalSystemConstraints&
        )
        AdimensionalSystemSolver(
            shared_ptr[DataContainer],
            const AdimensionalSystemConstraints&,
            const AdimensionalSystemSolution&
        )

        MiCPPerformance solve(VectorXd&, bool) nogil except +,
        AdimensionalSystemSolution get_raw_solution(VectorXd&)
        void run_pcfm(VectorXd&)
        void initialize_variables(VectorXd&, double, cumap[string, double]) except +
        void initialize_variables(VectorXd&, double, cumap[string, double], cumap[string, double]) except +
        void initialize_variables(VectorXd&, double, double) except +

        AdimensionalSystemSolverOptions& get_options() nogil # read-only


cdef extern from "conf_log.hpp" namespace "specmicp::io":
    void cpp_print_specmicp_options(AdimensionalSystemSolverOptions*) except+

# Wrapper to the solver
# ---------------------
cdef class SpecMiCPSolver:
    cdef DatabaseManager database
    cdef AdimensionalSystemSolver* solver
    cdef VectorXd* variables
    cdef bool is_solved

    cdef AdimensionalSystemSolverOptions* get_options(self) nogil
    cdef AdimensionalSystemOptions* get_system_options(self) nogil
    cdef MiCPSolverOptions* get_micpsolver_options(self) nogil
