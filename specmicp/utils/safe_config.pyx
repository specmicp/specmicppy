#Copyright (c) 2014,2016 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from cython.operator cimport dereference
from libcpp cimport nullptr_t, nullptr
from specmicp.utils.types cimport ensure_bytes

cdef class SafeConfigSection:
    """A configuration section"""
    def get_section(self, section):
        cdef SafeConfigSection new_sec = SafeConfigSection()
        cdef unique_ptr[YAMLConfigHandle] tmp_handle;

        cdef bytes b_section = ensure_bytes(section)
        sections = b_section.split(b'/')
        if (len(sections) > 1):
            tmp_handle.swap(get_sub_section(self.m_config.get(), sections));
        else:
            tmp_handle.swap(get_section(self.m_config.get(), b_section));

        if (sec_is_not_empty(tmp_handle)):
            new_sec.set_config(tmp_handle);
        else:
            raise RuntimeError("Section" + sections + " was not found.")
        return new_sec

    def is_valid(self):
        return (<bool> self.m_config)

cdef class SafeConfigFile:
    """A configuration file"""
    
    @staticmethod
    def read(filename):
        """Return a SafeConfigFile object containing the configuration from
        'filename'"""
        file = SafeConfigFile()
        cdef unique_ptr[YAMLConfigFile] tmp_ptr = YAMLConfigFile.make(ensure_bytes(filename))
        file.m_file.swap(tmp_ptr)
        if (sec_is_invalid(file.m_file.get())):
            raise RuntimeError("Invalid configuration file")
        return file

    @staticmethod
    def read_from_string(config_str):
        """Return a SafeConfigFile object containing the configuration in
        'config_str'"""
        file = SafeConfigFile()
        cdef unique_ptr[YAMLConfigFile] tmp_ptr = YAMLConfigFile.make_from_string(ensure_bytes(config_str));
        file.m_file.swap(tmp_ptr)

        return file

    def get_section(self, section):
        cdef SafeConfigSection new_sec = SafeConfigSection()
        cdef unique_ptr[YAMLConfigHandle] tmp_handle;

        cdef bytes b_section = ensure_bytes(section)
        sections = b_section.split(b'/')
        if (len(sections) > 1):
            tmp_handle.swap(get_sub_section(self.m_file.get(), sections))
        else:
            tmp_handle.swap(get_section(self.m_file.get(), b_section))
            
        if (sec_is_not_empty(tmp_handle)):
            new_sec.set_config(tmp_handle);
        else:
            raise RuntimeError("Section" + sections + " was not found.")
        return new_sec


    def is_valid(self):
        return (<bool> self.m_file)

