#Copyright (c) 2014,2016 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from libcpp cimport bool
from libcpp.string cimport string
from libcpp.vector cimport vector

from cython.operator cimport dereference

from memory cimport unique_ptr

cdef extern from "specmicp_common/io/safe_config.hpp" namespace "specmicp::io":
    cdef cppclass YAMLConfigHandle:
        bool is_map()
        bool is_sequence()

        bool has_section(const string&)
        YAMLConfigHandle get_section(const string&)

        YAMLConfigHandle(const YAMLConfigHandle&)

        YAMLConfigHandle() # should not be used

    cdef cppclass YAMLConfigFile(YAMLConfigHandle):
        @staticmethod
        load(const string&)
        @staticmethod
        load_from_string(const string&)

        @staticmethod
        unique_ptr[YAMLConfigFile] make(const string&)
        @staticmethod
        unique_ptr[YAMLConfigFile] make_from_string(const string&)

cdef extern from "safe_config_glue.hpp" namespace "specmicp::io":
    unique_ptr[YAMLConfigHandle] get_section(YAMLConfigHandle*, const string&) except+
    unique_ptr[YAMLConfigHandle] get_sub_section(YAMLConfigHandle*, vector[string]) except+
    YAMLConfigHandle&& get_right_ref(unique_ptr[YAMLConfigHandle] conf)
    bool sec_is_not_empty(unique_ptr[YAMLConfigHandle]&);
    bool sec_is_invalid(YAMLConfigHandle*);

cdef class SafeConfigSection:
    cdef unique_ptr[YAMLConfigHandle] m_config

    # note : this function take ownership of the pointer
    cdef inline set_config(self, unique_ptr[YAMLConfigHandle]& conf):
        self.m_config.swap(conf)



cdef class SafeConfigFile:
    cdef unique_ptr[YAMLConfigFile] m_file

    cdef inline set_file(self, unique_ptr[YAMLConfigFile]& file):
        self.m_file.swap(file)


