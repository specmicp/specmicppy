#Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from cython.operator cimport dereference

from specmicp.utils.types cimport eigen_vector_to_numpy_array
from specmicp.units cimport UnitsBox

import numbers

cdef class SpecMiCPSolution:
    """Contain a solution from the SpecMiCP solver

5    It correspond to the AdimensionalSystemSolutionExtractor of the c++ API

    This class should only be created by the SpecMiCP solver.
    """

    def __dealloc__(self):
        if self.extractor:
            del self.extractor
        if self.solution:
            del self.solution
    cdef void set_solution(
        self,
        const AdimensionalSystemSolution& solution,
        shared_ptr[DataContainer] database,
        UnitsSet units_set
        ):
        """Set the solution -- only callable from cython code !"""
        self.solution = new AdimensionalSystemSolution(solution)
        self.extractor = new AdimensionalSystemSolutionExtractor(
            dereference(self.solution),
            database,
            units_set
            )
        self.db = DatabaseManager(b"")
        self.db.init_database(self.extractor.get_database())

    cdef AdimensionalSystemSolution* _get(self):
        """Return the solution, only callable from cython code"""
        return self.solution

    cdef VectorXd get_main_variables(self):
        """Return the vector of main variables"""
        return self.extractor.get_main_variables()

        

    # water and gas volume fraction
    # -----------------------------
    def volume_fraction_water(self):
        """Return the volume fraction of water"""
        return self.extractor.volume_fraction_water()
    def density_water(self):
        """Return the density of water (in the correct units)"""
        return self.extractor.density_water()
    def porosity(self):
        """Return the porosity"""
        return self.extractor.porosity()
    def saturation_water(self):
        """Return the saturation of the liquid phase"""
        return self.extractor.saturation_water()
    def saturation_gas_phase(self):
        """Return the saturation of the gas phase"""
        return self.extractor.saturation_gas_phase()
    def volume_fraction_inert(self):
        """Return the volume fraction of inert phases"""
        return self.extractor.volume_fraction_inert()
    # solution properties
    # -------------------
    def pE(self):
        """Return pE"""
        return self.extractor.pE()
    def Eh(self):
        """Return Eh"""
        return self.extractor.Eh()
    def pH(self):
        """Return the pH of the solution"""
        return self.extractor.pH()
    def ionic_strength(self):
        """Return the ionic strength of the solution"""
        return self.extractor.ionic_strength()
    # components
    def molality_component(self, comp):
        """Return molality of component 'comp"""
        return self.extractor.molality_component(self.db.component_id(comp))
    def activity_component(self, comp):
        """Return the molality of component 'comp'"""
        return self.extractor.activity_component(self.db.component_id(comp))
    # minerals
    # --------
    def volume_fraction_mineral(self, mineral):
        """Return the volume fraction of mineral 'mineral'"""
        return self.extractor.volume_fraction_mineral(self.db.mineral_id(mineral))
    def mole_concentration_mineral(self, mineral):
        """Return the concentration (mol/V) of mineral 'mineral'"""
        return self.extractor.mole_concentration_mineral(self.db.mineral_id(mineral))
    def mass_concentration_mineral(self, mineral):
        """Return the concentration (M/V) of mineral 'mineral'"""
        return self.extractor.mass_concentration_mineral(self.db.mineral_id(mineral))
    def saturation_index(self, mineral):
        """Return the saturation index of mineral 'mineral'"""
        return self.extractor.saturation_index(self.db.mineral_id(mineral))
    def effective_saturation_index(self, mineral):
        """Return the effective saturation index of mineral 'mineral'"""
        return self.extractor.effective_saturation_index(self.db.mineral_id(mineral))
    def saturation_index_kinetic(self, mineral):
        """Return the saturation index of mineral 'mineral'"""
        return self.extractor.saturation_index_kinetic(self.db.mineral_kinetic_id(mineral))
    def effective_saturation_index_kinetic(self, index):
        """Return the effective saturation index of mineral 'index'"""
        return self.extractor.effective_saturation_index_kinetic(self.db.mineral_kinetic_id(index))
    # aqueous species
    # ---------------
    def molality_aqueous(self, aqueous):
        """Return the molality of aqueous 'aqueous'"""
        return self.extractor.molality_aqueous(self.db.aqueous_id(aqueous))
        return self.extractor.molality_aqueous(ida)
    def activity_aqueous(self, aqueous):
        """Return the activity of aqueous 'aqueous'"""
        return self.extractor.activity_aqueous(self.db.aqueous_id(aqueous))
    # gas
    # ---
    def fugacity_gas(self, gas):
        """Return the fugacity of gas 'gas'"""
        return self.extractor.fugacity_gas(self.db.gas_id(gas))
    # sorption model
    # --------------
    def concentration_free_surface_sites(self, index):
        """Return the concentration of free surface sites"""
        return self.extractor.free_surface_concentration(index)
    def molality_sorbed_species(self, index):
        """Return the molality of sorbed_species 'index'"""
        return self.extractor.molality_sorbed_species(index)
    # total concentrations
    # --------------------
    def total_concentration(self, comp):
        """Return the total concentration of component 'comp'."""
        return self.extractor.total_concentration(self.db.component_id(comp))

    def total_concentrations(self):
        """Return a numpy array of total concentrations"""
        return <object> eigen_vector_to_numpy_array(self.extractor.total_concentrations())

    def total_aqueous_concentration(self, comp):
        """Return the total aqueous concentration of component 'comp'

        /!\ molal concentration per amount of liquid phase
        """
        return self.extractor.total_aqueous_concentration(self.db.component_id(comp))
    def total_solid_concentration(self, comp):
        """Return the total solid concentration of component 'comp'"""
        return self.extractor.total_solid_concentration(self.db.component_id(comp))
    def total_immobile_concentration(self, comp):
        """Return the total immobile (solid+sorbed) concentration of component
        'comp'"""
        return self.extractor.total_immobile_concentration(self.db.component_id(comp))
    def total_mass_concentration(self):
        """Return the total mass concentration"""
        return self.extractor.total_mass_concentration()
    def total_solid_mass_concentration(self):
        """Return the total solid mass concentration"""
        return self.extractor.total_solid_mass_concentration()
    def total_liquid_mass_concentration(self):
        """Return the total solid mass concentration"""
        return self.extractor.mass_concentration_water()


    def get_database(self):
        """Return the database"""
        return self.db

    def main_variables(self):
        """Return the vector of main variables - debug"""
        return <object> eigen_vector_to_numpy_array(self.get_main_variables())


cpdef SpecMiCPSolution  parse_solution(bytes filepath, UnitsBox units_box):
    cdef shared_ptr[DataContainer] database
    cdef AdimensionalSystemSolution sol = parse_solution_yaml(filepath, database)
    cdef SpecMiCPSolution py_sol = SpecMiCPSolution()
    py_sol.set_solution(sol, database, dereference(units_box._get()))
    return py_sol

