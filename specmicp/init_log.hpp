/*-------------------------------------------------------------------------------

Copyright (c) 2014,2015 F. Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

-----------------------------------------------------------------------------*/

#ifndef SPECMICP_CYTHON_INITLOG_HPP
#define SPECMICP_CYTHON_INITLOG_HPP

#include <specmicp_common/log.hpp>
#include <iostream>

namespace specmicp {


inline void init_logger_cout_debug() {
    init_logger(&std::cout, specmicp::logger::Debug);
}

inline void init_logger_cout_warning() {
    init_logger(&std::cout, specmicp::logger::Warning);
}

inline void init_logger_cout_error() {
    init_logger(&std::cout, specmicp::logger::Error);
}

inline void init_logger_cerr_debug() {
    init_logger(&std::cerr, specmicp::logger::Debug);
}

inline void init_logger_cerr_warning() {
    init_logger(&std::cerr, specmicp::logger::Warning);
}

inline void init_logger_cerr_error() {
    init_logger(&std::cerr, specmicp::logger::Error);
}



} // end namespace specmicp

#endif // SPECMICP_CYTHON_INITLOG_HPP
