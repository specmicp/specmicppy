# Copyright (c) 2014-2017
#        Fabien Georget <fabieng@princeton.edu>, Princeton University 
# Copyright (c) 2018
#        Fabien Georget <fabien.georget@epfl.ch>, EPFL
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from cython.operator cimport dereference

import numpy as np
cimport numpy as np


# order is important here ... npy deprecated and such
from specmicp.utils.types cimport eigen_vector_to_numpy_array

"""Unsaturated driver output parser"""

cdef class UnsaturatedReader:
    """The Unsaturated HDF5 file reader"""
    def __cinit__(self,
                  bytes hdf5_path not None,
                  DatabaseManager db_manager not None
                  ):
        self.database = db_manager

        self.hdf5_reader = new UnsaturatedHDF5Reader(<string> hdf5_path)
        self.timesteps = &(self.hdf5_reader.get_timesteps())
        self.the_units = new UnitsSet(self.hdf5_reader.get_units())
        self.mesh = Mesh1D()
        self.mesh.set_mesh(self.hdf5_reader.get_mesh())

    def __dealloc__(self):
        del self.hdf5_reader
        del self.the_units

    def get_timesteps(self):
        """Return a numpy array containing the timesteps"""
        cdef np.ndarray[np.double_t, ndim=1] out = np.empty((self.timesteps.size(),),dtype=np.double)
        cdef size_t i
        for i in range(0, self.timesteps.size()):
            out[i] = self.timesteps.get(i)
        return out;

    def get_mesh(self):
        """Return the mesh."""
        return self.mesh

    def specmicp_solution(self, double timestep, int node):
        """Return the equilibrium solution from 'node' at 'timestep

        Raise a RuntimeError if the solution does not exist (e.g. : gas node)
        """
        the_sol = SpecMiCPSolution()
        the_sol.set_solution(
            self.hdf5_reader.get_adim_solution(
                self.timesteps.get_string(timestep),
                node
                ),
            self.database.get_raw_db(),
            dereference(self.the_units)
        )
        return the_sol

    def main_variable_vs_x(
        self
        , double timestep
        , bytes component
        , bytes variable
        ):
        """Return a numpy array containing the value of 'variable' for
        'component' at 'timestep'
        """
        return <object> eigen_vector_to_numpy_array(
            self.hdf5_reader.main_variable_vs_x( self.timesteps.get_string(timestep)
                                               , <string> component
                                               , <string> variable
                                               )
            )

    def main_variable_vs_t(self, int node, bytes component, bytes variable):
        """Return a numpy array containing the value of 'variable' for
        'component' at 'node' for every timestep
        """
        return <object> eigen_vector_to_numpy_array(
            self.hdf5_reader.main_variable_vs_t( node
                                               , <string> component
                                               , <string> variable
                                               )
            )

    def transport_variable_vs_x(
        self
        , double timestep
        , bytes variable
        ):
        """Return a numpy array containing the value of 'variable' for
        at 'timestep'
        """
        return <object> eigen_vector_to_numpy_array(
            self.hdf5_reader.transport_variable_vs_x(
                        self.timesteps.get_string(timestep)
                        , <string> variable
                        )
            )

    def transport_variable_vs_t(self, int node, bytes variable):
        """Return a numpy array containing the value of 'variable' for
        'component' at 'node' for every timestep
        """
        return <object> eigen_vector_to_numpy_array(
            self.hdf5_reader.transport_variable_vs_t( node
                                                    , <string> variable
                                                    )
            )


    def specmicp_variable_vs_t(self, int node, specmicp_solution_func, *args):
        """Return a numpy array containing the values obtain from the specmicp
        solution at each node. Values are obtained calling
        'specmicp_solution_func'

        'args' are extra argument to be passed to the specmicp_solution_func

        Example :
        >>> reader.specmicp_variable_vs_t(1, "l_mass_concentration_mineral", b"Portlandite")
        """
        cdef np.ndarray[np.double_t, ndim=1] out = np.empty((self.timesteps.size(),), dtype=np.double);
        cdef int i

        for i in range(0, self.timesteps.size()):
            sol = self.specmicp_solution(self.timesteps.get(i), node)
            func = getattr(sol, specmicp_solution_func)
            out[i] =  func(*args)

        return out

    def specmicp_variable_vs_x(self, double timestep, specmicp_solution_func, *args):
        """Return a numpy array containing the values obtain from the specmicp
        solution at each node. Values are obtained calling
        'specmicp_solution_func'

        'args' are extra argument to be passed to the specmicp_solution_func

        Example :
        >>> reader.specmicp_variable_vs_t(1, "l_mass_concentration_mineral", b"Portlandite")
        """
        cdef np.ndarray[np.double_t, ndim=1] out = np.empty((self.mesh.nb_nodes(),), dtype=np.double);
        cdef int i

        for n in range(0, self.mesh.nb_nodes()):
            sol = self.specmicp_solution(timestep, n)
            func = getattr(sol, specmicp_solution_func)
            out[n] =  func(*args)

        return out

