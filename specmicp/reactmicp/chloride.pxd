# Copyright (c) 2014-2017
#        Fabien Georget <fabieng@princeton.edu>, Princeton University 
# Copyright (c) 2018
#        Fabien Georget <fabien.georget@epfl.ch>, EPFL
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


from libcpp.string cimport string
from specmicp.units cimport UnitsSet
from specmicp.database cimport DatabaseManager,DataContainer
from specmicp.solution cimport AdimensionalSystemSolution
from specmicp.reactmicp.hdf5_timesteps cimport HDF5Timesteps
from memory cimport shared_ptr
from specmicp.dfpm.mesh cimport Mesh1D, CppMesh1D
from eigen cimport VectorXd


cdef extern from "reactmicp/io/hdf5_chloride.hpp" namespace "specmicp::io":
    cdef cppclass ChlorideHDF5Reader:
        ChlorideHDF5Reader(const string& filepath, shared_ptr[DataContainer] raw_data) except +
        
        const HDF5Timesteps& get_timesteps() const;
        UnitsSet get_units();
        
        AdimensionalSystemSolution get_adim_solution(double, int) except +
        
        VectorXd main_variable_vs_x(const string&, int, const string&)
        VectorXd main_variable_vs_x(double, int, const string&)
        VectorXd total_mobile_concentration_vs_x(double, const string&)
        VectorXd potential_vs_x(double)
        
        VectorXd main_variable_vs_t(int, int, const string&)
        VectorXd total_mobile_concentration_vs_t(int, const string&)
        VectorXd potential_vs_t(double)
        VectorXd current_vs_t(int)
        
        shared_ptr[CppMesh1D] get_mesh() except +
        
cdef class ChlorideReader:
    cdef DatabaseManager database
    cdef ChlorideHDF5Reader* hdf5_reader
    cdef const HDF5Timesteps* timesteps
    cdef UnitsSet* the_units
    cdef Mesh1D mesh    
        
        