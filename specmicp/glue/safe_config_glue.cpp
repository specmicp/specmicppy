/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "safe_config_glue.hpp"
#include "specmicp_common/compat.hpp"


#include "specmicp/problem_solver/reactant_box.hpp"
#include "specmicp/io/configuration.hpp"
#include "specmicp_database/io/configuration.hpp"

namespace specmicp {
namespace io {

std::unique_ptr<YAMLConfigHandle> get_section(
        YAMLConfigHandle* conf,
        const std::string& name
        )
{
    if (sec_is_invalid(conf)) {
        throw std::runtime_error("Error while trying to obtain sub-section "
                                 + name + ", from invalid conf");
    }
    if (not conf->has_section(name)) {
        throw std::runtime_error("No section name 'name'");
    }
    return std::unique_ptr<YAMLConfigHandle>(new YAMLConfigHandle(conf->get_section(name)));
}

std::unique_ptr<YAMLConfigHandle> get_sub_section(
        YAMLConfigHandle* conf,
        std::vector<std::string> names
        )
{
    std::unique_ptr<YAMLConfigHandle> sec = get_section(conf, names[0]);
    for (auto i=1; i<names.size(); ++i)
    {
        std::unique_ptr<YAMLConfigHandle> tmp = get_section(sec.get(), names[i]);
        tmp.swap(sec);
    }
    return sec;
}

std::shared_ptr<specmicp::database::DataContainer> configure_database_glue(
        YAMLConfigHandle* conf,
        std::vector<std::string>& dirs
        )
{
    return configure_database(std::move(*conf), dirs);
}

void configure_reactant_box(
        ReactantBox* react_box,
        std::unique_ptr<YAMLConfigHandle>& conf
        )
{
    configure_specmicp_reactant_box(*react_box, std::move(*conf.release()));
}

} // end namespace io
} // end namespace specmicp
