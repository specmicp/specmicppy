# Copyright (c) 2014-2017
#        Fabien Georget <fabieng@princeton.edu>, Princeton University 
# Copyright (c) 2018
#        Fabien Georget <fabien.georget@epfl.ch>, EPFL
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from specmicp.units cimport UnitsBox
from specmicp.constraints cimport SpecMiCPConstraints

from specmicp.utils.safe_config cimport SafeConfigFile, SafeConfigSection
from specmicp.utils.types cimport list_to_eigen, ensure_bytes

from memory cimport unique_ptr
from cython.operator cimport dereference

def configure_reactant_box(
        DatabaseManager db_manager not None,
        UnitsBox units_box not None,
        SafeConfigFile configuration not None,
        section not None):
    reactant_box = ReactantBox(db_manager, units_box)
    cdef SafeConfigSection sec = configuration.get_section(section)
    cpp_configure_reactant_box(
            reactant_box.c_box,
            sec.m_config)
    return reactant_box

cdef class ReactantBox:
    def __cinit__(self,
                  DatabaseManager db_manager not None,
                  UnitsBox the_units not None
                  ):
        self.db_manager = db_manager
        self.c_box = new CppReactantBox(
            db_manager.get_raw_db(),
            dereference(the_units._get())
        )
    def __dealloc__(self):
        del self.c_box

    def set_solution(self, double value, unit):
        """Set the solution amount"""
        self.c_box.set_solution(value, ensure_bytes(unit))

    def add_aqueous_species(self, species, double value, unit):
        """Add an aqueous species to the system"""
        self.c_box.add_aqueous_species(
                ensure_bytes(species), value, ensure_bytes(unit))

    def set_aqueous_species(self, species, double value, unit):
        """Set the amount of an aqueous species to the system"""
        self.c_box.set_aqueous_species(
                ensure_bytes(species), value, ensure_bytes(unit))

    def add_solid_phase(self, species, double value, unit):
        """Add a solid phase to the system"""
        self.c_box.add_solid_phase(
                ensure_bytes(species), value, ensure_bytes(unit))

    def set_solid_phase(self, species, double value, unit):
        """Set the amount of a solid phase to the system"""
        self.c_box.set_solid_phase(
                ensure_bytes(species), value, ensure_bytes(unit))

    def add_component(self, component):
        """Add a component to the system"""
        self.c_box.add_component(ensure_bytes(component))

    def set_charge_keeper(self, component):
        """Set the charge keeper"""
        self.c_box.set_charge_keeper(ensure_bytes(component))

    def set_inert_volume_fraction(self, double value):
        """Set the inert volume fraction"""
        self.c_box.set_inert_volume_fraction(value)

    def set_saturated_system(self):
        """The system is saturated"""
        self.c_box.set_saturated_system()

    def disable_conservation_water(self):
        """Disable the conservation of the water in the system"""
        self.c_box.disable_conservation_water()

    def set_fixed_saturation(self, value):
        """The saturation of the system is fixed at 'value'."""
        self.c_box.set_fixed_saturation(value)

    def add_fixed_fugacity_gas(self, gas, component, double value):
        """Set gas to have a fixed fugacity"""
        self.c_box.add_fixed_fugacity_gas(
                ensure_bytes(gas), ensure_bytes(component), value)

    def add_fixed_activity_component(self, component, double value):
        """Set the component to have a fixed activity"""
        self.c_box.add_fixed_activity_component(
                ensure_bytes(component), value)

    def add_fixed_molality_component(self, component, double value):
        """Set the component to have a fixed molality"""
        self.c_box.add_fixed_molality_component(
                ensure_bytes(component), value)

    def add_fixed_saturation_index(self, mineral, component, double value=0.0):
        """Set a mineral to have a fixed saturation index"""
        self.c_box.add_fixed_saturation_index(
                ensure_bytes(mineral), ensure_bytes(component), value)

    def disable_surface_model(self):
        """Disable the surface sorption model"""
        self.c_box.disable_surface_model()
        
    def set_equilibrium_surface_model(self, list ssites_conc):
        """Set the surface sorption model to be equilibrium"""
        self.c_box.set_equilibrium_surface_model(list_to_eigen(ssites_conc))

    def set_EDL_surface_model(self, list ssites_conc, double sorption_surface):
        """Set the surface sorption model to the double layer model"""
        self.c_box.set_EDL_surface_model(list_to_eigen(ssites_conc), sorption_surface)

    def disable_immobile_species(self):
        """Disable immobile species in specmicp solver"""
        self.c_box.disable_immobile_species()

    def get_constraints(self, modify_database=False):
        """Return the constraints"""
        constraints = SpecMiCPConstraints(self.db_manager)
        constraints._set(self.c_box.get_constraints(modify_database))
        return constraints
