# Copyright (c) 2014-2016
# Fabien Georget <fabieng@princeton.edu>, Princeton University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from cython.operator cimport dereference
from specmicp.utils.safe_config cimport SafeConfigFile
from specmicp.utils.types cimport ensure_bytes

def configure_units(
        SafeConfigFile conf not None,
        section not None):
    units_box = UnitsBox()
    cdef UnitsSet units_set = cpp_configure_units(
            conf.m_file.get().get_section(ensure_bytes(section)))
    units_box._set(units_set)
    return units_box


cdef class UnitsBox:
    """A set of units"""
    def __cinit__(self):
        self.c_units = new UnitsSet()

    def __dealloc__(self):
        del self.c_units

    cdef const UnitsSet* _get(self):
        """Return a pointer to the units"""
        return self.c_units


    cdef _set(self, const UnitsSet& units_set):
        """Set the units from a c set of units"""
        del self.c_units
        self.c_units = new UnitsSet(units_set)


    property length:
        """The units for length"""
        def __get__(self):
            return to_string(self.c_units.length)
        def __set__(self,  value):
            if value in ["meter", "m", b"meter", b"m"]:
                self.c_units.length = u_meter
            elif value in ["decimeter", "dm", b"decimeter", b"dm"]:
                self.c_units.length = u_decimeter
            elif value in ["centimeter", "cm", b"centimeter", b"cm"]:
                self.c_units.length = u_centimeter
            else:
                raise ValueError("Unknown unit for length : '{0}'".format(value))


    property mass:
        """The units for mass"""
        def __get__(self):
            return to_string(self.c_units.mass)
        def __set__(self, value):
            if value in ["kg", "kilogram", b"kg", b"kilogram"]:
                self.c_units.mass = u_kilogram
            elif value in ["g", "gram", b"g", b"gram"]:
                self.c_units.mass = u_gram
            else:
                raise ValueError("Unknown unit for mass : '{0}'".format(value))

    property quantity:
        """The units for the quantity of matters"""
        def __get__(self):
            return to_string(self.c_units.quantity)
        def __set__(self, value):
            if value in ["mol", "moles", b"mol", b"moles"]:
                self.c_units.quantity = u_moles
            elif value in ["mmol", "millimoles", b"mmol", b"millimoles"]:
                self.c_units.quantity = u_millimoles
            else:
                raise ValueError("Unknown unit for quantity of matter : '{0}'".format(value))

def get_SI_units():
    t_units = UnitsBox()
    t_units._set(SI_units)
    return t_units

def get_CMG_units():
    t_units = UnitsBox()
    t_units._set(CMG_units)
    return t_units