# Copyright (c) 2014-2016
# Fabien Georget <fabieng@princeton.edu>, Princeton University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from libcpp cimport nullptr

"""Access the mesh module of dfpm"""

cdef class Mesh1D:
    """A 1D mesh"""
    def __cinit__(self):
        self.cpp_mesh = <shared_ptr[CppMesh1D]> nullptr

    cdef CppMesh1D* get_ptr(self):
        return self.cpp_mesh.get()

    cdef set_mesh(self, shared_ptr[CppMesh1D] amesh):
        self.cpp_mesh = amesh

    def nb_nodes(self):
        """Return the number of nodes"""
        return self.get_ptr().nb_nodes()

    def nb_elements(self):
        """Return the number of elements"""
        return self.get_ptr().nb_elements()

    def get_position(self, int node):
        """Return the coordinates of a node"""
        return self.get_ptr().get_position(node)

    def get_dx(self, int element):
        """Return the length of an element"""
        return self.get_ptr().get_dx(element)

    def get_face_area(self, int element):
        """Return the area of the face between two nodes in an element"""
        return self.get_ptr().get_face_area(element)

    def get_volume_element(self, int element):
        """Return the volume of an element"""
        return self.get_ptr().get_volume_element(element)

    def get_volume_cell(self, int node):
        """Return the volume of a cell (volume around a node)."""
        return self.get_ptr().get_volume_cell(node)

    def get_volume_cell_element(self, int element, int enode):
        """Return the volume of a cell inside an element, (i.e. the volume
        of a node inside an element)"""
        return self.get_ptr().get_volume_cell_element(element, enode)

    def integrate_array(self, double[:] arr):
        """Volume integration of the values given in array. Array must have one
        value per node"""
        cdef double s = 0
        cdef int i = 0
        for i in range(0, self.get_ptr().nb_nodes()):
            s += arr[i]*self.get_ptr().get_volume_cell(i)
        return s

    def integrate_function(self, func, *extra_args):
        """
        Volume integration of func.
        func must have the signature func(node, *extra_args).
        """
        cdef double s = 0
        cdef int i = 0
        for i in range(0, self.get_ptr().nb_nodes()):
            s += func(i, *extra_args)*self.get_ptr().get_volume_cell(i)
        return s

cpdef get_uniform_mesh(int nb_nodes, double dx, double section):
    """Return an uniform 1D mesh"""
    cdef Uniform1DMeshGeometry geom
    geom.nb_nodes = nb_nodes
    geom.dx = dx
    geom.section = section

    cdef shared_ptr[CppMesh1D] amesh = uniform_mesh1d(geom)
    cdef Mesh1D the_mesh = Mesh1D()
    the_mesh.set_mesh(amesh)
    return the_mesh
