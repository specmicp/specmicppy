# Copyright (c) 2014-2016
# Fabien Georget <fabieng@princeton.edu>, Princeton University
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from memory cimport shared_ptr

cdef extern from "dfpm/meshes/mesh1d.hpp" namespace "specmicp::mesh":
    cdef cppclass CppMesh1D "specmicp::mesh::Mesh1D":
        int nb_elements()
        int nb_nodes()

        double get_position(int)
        double get_dx(int)
        double get_face_area(int)
        double get_volume_element(int)
        double get_volume_cell(int)
        double get_volume_cell_element(int, int)

    cdef cppclass Uniform1DMeshGeometry:
        double dx
        int nb_nodes
        double section

    cdef shared_ptr[CppMesh1D] uniform_mesh1d(const Uniform1DMeshGeometry)

cdef class Mesh1D:
    cdef shared_ptr[CppMesh1D] cpp_mesh
    cdef CppMesh1D* get_ptr(self)
    cdef set_mesh(self, shared_ptr[CppMesh1D] amesh)