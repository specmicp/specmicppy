#Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



from eigen cimport VectorXd
from libcpp.map cimport map as cmap
from libcpp.string cimport string
from libcpp.vector cimport vector

from memory cimport shared_ptr
from specmicp.database cimport DatabaseManager, DataContainer
from specmicp.units import UnitsSet

# The constraints
# ---------------
cdef extern from "specmicp/adimensional/adimensional_system_structs.hpp" namespace "specmicp":
    cdef cppclass AdimensionalSystemConstraints:
        AdimensionalSystemConstraints()
        AdimensionalSystemConstraints(const AdimensionalSystemConstraints& other)

        VectorXd total_concentrations

        void set_total_concentrations(const VectorXd&)

        void enable_conservation_water()
        void disable_conservation_water()
        void set_saturated_system()

        void disable_surface_model()
        void set_equilibrium_surface_model(VectorXd)
        void set_EDL_surface_model(VectorXd, float);


        void set_charge_keeper(int)
        void add_fixed_fugacity_gas(int, int, float)
        void add_fixed_activity_component(int, double)

        void set_inert_volume_fraction(double)
        void disable_immobile_species()

# The formulation of the problem
# ------------------------------
cdef extern from "specmicp/problem_solver/formulation.hpp" namespace "specmicp":
    cdef cppclass Formulation:
        Formulation()
        void set_mass_solution(double)
        void add_aqueous_species(const string&, double) except +
        void add_mineral(const string&, double) except +
        void keep_component(const string&)
        void set_minerals_list(const vector[string]& list)

# Dissolve the formulation
# ------------------------
cdef extern from "specmicp/problem_solver/dissolver.hpp" namespace "specmicp":
    cdef cppclass Dissolver:
        Dissolver(shared_ptr[DataContainer])
        VectorXd dissolve(const Formulation&) except +
        VectorXd get_total_concentration()

# Wrapper for the constraints
# ---------------------------
cdef class SpecMiCPConstraints:
    cdef DatabaseManager database
    cdef AdimensionalSystemConstraints* constraints
    cdef AdimensionalSystemConstraints* _get(self)
    cdef _set(self, const AdimensionalSystemConstraints&)

# Wrapper for the formulation
# ---------------------------
cdef class SpecMiCPFormulation(SpecMiCPConstraints):
    cdef Formulation* formulation


cdef extern from "conf_log.hpp" namespace "specmicp::io":
    void cpp_print_specmicp_constraints(
        shared_ptr[DataContainer] db,
        AdimensionalSystemConstraints* constraints)

# ReactantBox : new and better dissolver
# ======================================

# look at module : specmicp.reactant_box