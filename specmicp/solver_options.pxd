#Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from libcpp cimport bool
from specmicp.units cimport UnitsSet

cdef extern from "specmicp_common/micpsolver/micpsolver_structs.hpp" namespace "specmicp::micpsolver":
    cdef cppclass MiCPSolverOptions:
        MiCPSolverOptions()
        int max_iter
        double fvectol
        double steptol
        double condition_limit
        double penalization_factor
        double maxstep
        int maxiter_maxstep
        double factor_descent_condition
        double power_descent_condition
        bool use_crashing
        double coeff_accept_newton_step
        bool use_scaling
        double factor_gradient_search_direction
        double projection_min_variable
        double threshold_stationary_point
        int max_factorization_step

        void set_maximum_iterations(int)
        void set_maximum_step_length(double)
        void set_maximum_step_length(double, int)
        void disable_descent_direction()
        void enable_descent_direction(double, double)
        void disable_condition_check()
        void enable_condition_check(double)
        void disable_non_monotone_linesearch()
        void enable_scaling()
        void disable_scaling()
        void enable_crashing()
        void disable_crashing()
        void enable_non_monotone_linesearch()
        void set_tolerance(double)
        void set_tolerance(double, double)


    cdef enum MiCPSolverReturnCode:
        LolItsNotSupposedToHappen "specmicp::micpsolver::MiCPSolverReturnCode::LolItsNotSupposedToHappen"
        MaxStepTakenTooManyTimes "specmicp::micpsolver::MiCPSolverReturnCode::MaxStepTakenTooManyTimes"
        FailedToSolveLinearSystem "specmicp::micpsolver::MiCPSolverReturnCode::FailedToSolveLinearSystem"
        MaxIterations "specmicp::micpsolver::MiCPSolverReturnCode::MaxIterations"
        StationaryPoint "specmicp::micpsolver::MiCPSolverReturnCode::StationaryPoint"
        NotConvergedYet "specmicp::micpsolver::MiCPSolverReturnCode::NotConvergedYet"
        Success "specmicp::micpsolver::MiCPSolverReturnCode::Success"
        ResidualMinimized "specmicp::micpsolver::MiCPSolverReturnCode::ResidualMinimized"
        ErrorMinimized "specmicp::micpsolver::MiCPSolverReturnCode::ErrorMinimized"

    cdef cppclass MiCPPerformance:
        MiCPPerformance()
        int nb_call_residuals
        int nb_call_jacobian
        int nb_factorization
        int nb_gradient_step
        int nb_crashing_iterations
        int nb_iterations
        bool max_taken
        int nb_max_taken
        int nb_consecutive_max_taken
        MiCPSolverReturnCode return_code

    cdef enum NCPfunction:
        penFB_function "specmicp::micpsolver::NCPfunction::penalizedFB"
        min_function "specmicp::micpsolver::NCPfunction::min"

cdef extern from "is_solver_successful.hpp" namespace "specmicp::micpsolver":
    cdef bool is_solver_successful(MiCPSolverReturnCode)
    cdef int cast_return_code(MiCPSolverReturnCode)

cdef extern from "specmicp/adimensional/adimensional_system_structs.hpp" namespace "specmicp":
    cdef cppclass AdimensionalSystemOptions:
        bool non_ideality
        int non_ideality_max_iter
        double scaling_electron
        double non_ideality_tolerance
        double under_relaxation_factor
        double restart_concentration
        double new_component_concentration
        double start_non_ideality_computation
        double cutoff_total_concentration

cdef extern from "specmicp/adimensional/adimensional_system_solver_structs.hpp" namespace "specmicp":
    cdef cppclass AdimensionalSystemSolverOptions:
        AdimensionalSystemSolverOptions()
        MiCPSolverOptions solver_options
        AdimensionalSystemOptions system_options
        UnitsSet units_set
        bool allow_restart
        bool use_pcfm
        bool force_pcfm

