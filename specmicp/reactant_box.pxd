# Copyright (c) 2014-2017
#        Fabien Georget <fabieng@princeton.edu>, Princeton University 
# Copyright (c) 2018
#        Fabien Georget <fabien.georget@epfl.ch>, EPFL
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

"""Interface to a ReactantBox, initial formulation of a system"""

from libcpp cimport bool
from libcpp.string cimport string

from memory cimport shared_ptr, unique_ptr
from eigen cimport VectorXd

from cython.operator cimport dereference

from specmicp.database cimport DataContainer, DatabaseManager
from specmicp.units cimport UnitsSet
from specmicp.constraints cimport AdimensionalSystemConstraints
from specmicp.utils.safe_config cimport YAMLConfigHandle


cdef extern from "specmicp/problem_solver/reactant_box.hpp" namespace "specmicp":
    cdef cppclass CppReactantBox "specmicp::ReactantBox":
        CppReactantBox(shared_ptr[DataContainer], const UnitsSet&) except +
        CppReactantBox(const CppReactantBox&)

        void set_solution(double, string) except +
        void add_aqueous_species(string, double, string) except +
        void add_solid_phase(string, double, string) except +
        void set_aqueous_species(string, double, string) except +
        void set_solid_phase(string, double, string) except +
        void add_component(string) except +

        VectorXd get_total_concentration(bool) except +

        void set_charge_keeper(string) except +
        void set_inert_volume_fraction(double) except +
        void set_saturated_system() except +
        void disable_conservation_water() except +
        void set_fixed_saturation(double) except+

        void add_fixed_fugacity_gas(string, string, double) except +
        void add_fixed_activity_component(string, double) except +
        void add_fixed_molality_component(string, double) except +
        void add_fixed_saturation_index(string, string, double) except +
        
        void disable_surface_model() except +
        void set_equilibrium_surface_model(VectorXd) except +
        void set_EDL_surface_model(VectorXd, double) except +
        
        void disable_immobile_species() except +

        AdimensionalSystemConstraints get_constraints(double) except +

#cdef extern from "specmicp/io/configuration.hpp" namespace "specmicp::io":
#    cdef void cpp_configure_reactant_box "specmicp::io::configure_specmicp_reactant_box" (
#        CppReactantBox&,
#        YAMLConfigHandle&&) except +

cdef extern from "safe_config_glue.hpp" namespace "specmicp::io":
    cdef void cpp_configure_reactant_box "specmicp::io::configure_reactant_box" (
        CppReactantBox*,
        unique_ptr[YAMLConfigHandle]) except +

cdef class ReactantBox:
    cdef CppReactantBox* c_box
    cdef DatabaseManager db_manager


