#Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from libcpp.pair cimport pair
from cython.operator import dereference as deref
from specmicp.units cimport UnitsBox

from specmicp.utils.safe_config cimport SafeConfigFile, SafeConfigSection, sec_is_invalid
from specmicp.utils.types cimport ensure_bytes, ensure_str
import os
import numbers

# Used to indicate that something does not exist
cdef enum:
    no_species = -1


def configure_database(
        SafeConfigFile conf_file not None,
        bytes section not None):

    db_manager = DatabaseManager(None) 
    cdef vector[string] path_dirs
    if os.environb[b'SPECMICP_DATABASE_PATH']:
        dirs = os.environb[b'SPECMICP_DATABASE_PATH'].split(b';')
        path_dirs.reserve(len(dirs))
        for adir in dirs:
            path_dirs.push_back(<string> adir)
    
    if (sec_is_invalid(conf_file.m_file.get())):
        raise RuntimeError("Invalid configuration file")
        
    cdef SafeConfigSection sec = conf_file.get_section(section)
    cdef shared_ptr[DataContainer] raw_data = configure_database_glue(sec.m_config.release(), path_dirs)
    del sec
    
    db_manager.init_database(raw_data)

    return db_manager

cdef class DatabaseManager:
    """ The Python database handler

    Use this class for checking values in the database
    or switching basis/dropping minerals or components...
    """
    def __cinit__(self, filepath, bool check_compo=False):
        """filepath is the path to the database file"""
        if filepath:
            self.database = new Database(ensure_bytes(filepath), check_compo)
        else:
            self.database = new Database()
        self.container = self.database.get_database()
    def __dealloc__(self):
        del self.database
    cdef void init_database(self, shared_ptr[DataContainer] raw_db):
        del self.database
        self.database = new Database(raw_db)
        self.container = self.database.get_database()
    cdef shared_ptr[DataContainer] get_raw_db(self):
        return self.container
    cdef DataContainer* _get(self):
        return self.container.get()


    def save_database(self, filename):
        """Save the database in a file"""
        self.database.save(ensure_bytes(filename))

    def is_valid(self):
        """Return true of the database is valid.

        If it isn't valid, something went wrong.
        """
        return self.database.is_valid();

    def is_index(self, ids):
        """Return true if ids is an index"""
        return isinstance(ids, numbers.Integral)

    def remove_sorbed_species(self):
        """Remove all the sorbed species in the database."""
        self.database.remove_sorbed_species()

    def add_sorbed_species(self, input_sorbed):
        """ Add sorbed species in the database

        'input_sporbed' is a JSON list of sorbed species formatted as in the
        database
        """
        self.database.add_sorbed_species(ensure_bytes(input_sorbed))

    # --------- #
    # Component #
    # --------- #

    property nb_component:
        """The number of component in the database."""
        def __get__(self): return self._get().nb_component()

    def component_check_bounds(self, int idc):
        """Check if 'idc' is a correct index for a component.

        Raises a ValueError if it is not correct."""
        if (idc >= self.nb_component or idc < 0):
            raise ValueError("'"+str(idc)+
                             "' is not a valid index for a component")


    # labels and id
    # --------------
    def component_id(self, comp):
        """Return the id of 'comp'"""
        if self.is_index(comp):
            idc = comp
        else:
            idc = self._get().get_id_component(ensure_bytes(comp))
        if (idc == no_species or idc >= self.nb_component):
            raise ValueError("Species : '"+ensure_str(comp)+"' is not a valid component.")
        return idc
    def component_label_to_id(self, comp):
        """ Return the id of a component."""
        return self.component_id(comp)
    def component_id_to_label(self, int idc):
        """ Return the label of a component."""
        if (idc >= self.nb_component):
            raise ValueError("'"+str(idc)+
                             "' is not a valid index for a component")
        return bytes(self._get().get_label_component(idc)).decode()

    def print_basis(self):
        """Print the components in the basis, with their ID"""
        print("The basis is :")
        cdef int i
        for i in range(self.nb_component):
            print(" - "+str(i)+" : "+self.component_id_to_label(i))


    # properties
    # ----------
    def molar_mass_component(self, comp, UnitsBox units_box=None):
        """Return the molar mass of a component.
        
        In SI units if units_box is not provided
        """
        idc = self.component_label_to_id(comp)
        if units_box is None:
            return self._get().molar_mass_basis(idc)
        else:
            return self._get().molar_mass_basis(idc, <const UnitsSet&>  deref(units_box._get()))

    # basis switch
    # ------------

    def swap_components(self, dict swapping):
        """Swap components in the basis

        swapping is a dictionnary where the keys are the labels of the
        components to replace and the values are the labels of the secondary
        species to add in the basis.

        Raise value errors if the labels are invalid
        """
        cdef cmap[string,string] input_map
        cdef int idsp
        for (key, value) in swapping.items():
            idsp = self.component_label_to_id(ensure_bytes(key))
            if (idsp == no_species):
                raise ValueError("'"+ensure_str(key)+"' is not a valid component.")
            idsp = self.aqueous_label_to_id(ensure_bytes(value))
            if (idsp == no_species):
                raise ValueError("'"+ensure_str(key)+
                                 "' is not a valid secondary aqueous species.")
            input_map.insert(pair[string, string](ensure_bytes(key), ensure_bytes(value)))
        self.database.swap_components(input_map)

    # --------------- #
    # Aqueous species #
    # --------------- #

    property nb_aqueous:
        """The number of secondary aqueous species in the database"""
        def __get__(self): return self._get().nb_aqueous()

    def aqueous_check_bounds(self, int ida):
        """Check if 'ida' is a correct index for a secondary aqueous species.

        Raises a ValueError if it is not correct."""
        if (ida >= self.nb_aqueous or ida < 0):
            raise ValueError("'"+str(ida)+
                             "' is not a valid index for a secondary aqueous species")

    # labels and id
    # -------------
    def aqueous_id(self, aqueous):
        """Return the id of 'aqueous'"""
        if self.is_index(aqueous):
            ida = aqueous
        else:
            ida = self._get().get_id_aqueous(ensure_bytes(aqueous))
        if (ida == no_species or ida >= self.nb_aqueous):
            raise ValueError("Species : '"+ensure_str(aqueous)+"' is not a valid aqueous species.")
        return ida
    def aqueous_label_to_id(self, aqueous):
        """ Return the id of a secondary aqueous species"""
        return self.aqueous_id(aqueous)
    def aqueous_id_to_label(self, int ids):
        self.aqueous_check_bounds(ids)
        return bytes(self._get().get_label_aqueous(ids)).decode()

    
    def print_aqueous(self):
        """ Print the secondary species"""
        print("The secondary aqueous species are :")
        cdef int i
        for i in range(self.nb_aqueous):
            print(" - "+str(i)+" : "+self.aqueous_id_to_label(i))

    # properties
    # ----------

    def nu_aqueous(self, aqueous, comp):
        """Return stoichiometric coefficient of a secondary aqueous species"""
        return self._get().nu_aqueous(
                self.aqueous_id(aqueous),
                self.component_id(comp)
                )
                
    def logk_aqueous(self, aqueous):
        """Return the equilibrium constant of a secondary aqueous species"""
        return self._get().logk_aqueous(self.aqueous_id(aqueous))

    # ------------ #
    # Solid phases #
    # ------------ #

    def remove_solid_phases(self):
        """Remove all the solid phases in the database.
        """
        self.database.remove_solid_phases()

    def add_solid_phases(self, input_solid_phases):
        """Add some solid phases in the database.

        'input_sorbed' is a JSON list of minerals formatted as in the database
        """
        self.database.add_solid_phases(ensure_bytes(input_solid_phases))

    # Equilibrium
    # =======

    property nb_mineral:
        """The number of mineral in the database"""
        def __get__(self):
            return self._get().nb_mineral()

    def mineral_check_bounds(self, int idm):
        """Check if 'idm' is a correct index for a mineral.

        Raises a ValueError if it is not correct."""
        if (idm >= self.nb_mineral or idm < 0):
            raise ValueError("'"+str(idm)+
                             "' is not a valid index for a mineral")


    # labels and id
    # -------------
    def mineral_id(self, mineral):
        """Return the id of 'mineral'"""
        if self.is_index(mineral):
            idm = mineral
        else:
            idm = self._get().get_id_mineral(ensure_bytes(mineral))
        if (idm == no_species or idm >= self.nb_mineral):
            raise ValueError("Species : '"+ensure_str(mineral)+"' is not a valid solid phase at equilibrium.")
        return idm
    def mineral_id_to_label(self, int idm):
        """Return the label of a mineral."""
        self.mineral_check_bounds(idm)
        return bytes(self._get().get_label_mineral(idm)).decode()
    def mineral_label_to_id(self, mineral):
        """ Return the id of a mineral"""
        return self.mineral_id(mineral)

    def print_minerals(self):
        """ Print the solid phases at equilibrium"""
        print("The solid phases at equilibrium are :")
        cdef int i
        for i in range(self.nb_mineral):
            print(" - "+str(i)+" : "+self.mineral_id_to_label(i))

    def minerals_keep_only(self, list_mineral_to_keep):
        """Keep only the solid phases in 'list_mineral_to_keep'"""
        # we first verify that the list is valid
        new_list = []
        for label in list_mineral_to_keep:
            self.mineral_label_to_id(label) # just check that it is not raising any error
            new_list.append(ensure_bytes(label))
        self.database.minerals_keep_only(new_list)

    # properties
    # ----------

    def logk_mineral(self, mineral):
        """Return the equilibrium constant of a solid phase"""
        return self._get().logk_mineral(self.mineral_id(mineral))

    def nu_mineral(self, mineral, comp):
        """Return stoichometric coefficient of a mineral"""
        return self._get().nu_mineral(
                self.mineral_id(mineral),
                self.component_id(comp)
                )
    
    def molar_mass_mineral(self, mineral, UnitsBox units_box=None):
        """Return the molar mass (kg/mol by default) of mineral 'idm'"""
        idm = self.mineral_id(mineral)
        if units_box is None:
            return self._get().molar_mass_mineral(idm)
        else:
            return self._get().molar_mass_mineral(idm, <const UnitsSet&>  deref(units_box._get()))

    def molar_volume_mineral(self, mineral, UnitsBox units_box=None):
        """Return the molar volume (m^3/mol by default) of mineral 'idm'"""
        idm = self.mineral_id(mineral)
        if units_box is None:
            return self._get().molar_volume_mineral(idm)
        else:
            return self._get().molar_volume_mineral(idm, <const UnitsSet&> deref(units_box._get()))

    def density_mineral(self, mineral, UnitsBox units_box=None):
        """Return the density (kg/m^3) of a mineral 'idm'"""
        idm = self.mineral_id(mineral)
        if units_box is None:
            return (self._get().molar_mass_mineral(idm)
                / self._get().molar_volume_mineral(idm)
                )
        else:
            return (self._get().molar_mass_mineral(idm, deref(units_box._get()))
                / self._get().molar_volume_mineral(idm, deref(units_box._get()))
                )

    # Kinetic
    # ===========

    property nb_mineral_kinetic:
        """The number of mineral governed by kinetics in the database"""
        def __get__(self):
            return self._get().nb_mineral_kinetic()

    def mineral_kinetic_check_bounds(self, int idm):
        """Check if 'idm' is a correct index for a kinetic mineral.

        Raises a ValueError if it is not correct."""
        if (idm >= self.nb_mineral_kinetic or idm < 0):
            raise ValueError("'"+str(idm)+
                             "' is not a valid index for a kinetic mineral")


    # labels and id
    # -------------
    def mineral_kinetic_id(self, mineral):
        """Return the id of 'mineral'"""
        if self.is_index(mineral):
            idm = mineral
        else:
            idm = self._get().get_id_mineral_kinetic(ensure_bytes(mineral))
        if (idm == no_species or idm >= self.nb_mineral_kinetic):
            raise ValueError("Species : '"+ensure_str(mineral)+"' is not a valid solid phase governed by kinetics.")
        return idm
    def mineral_kinetic_id_to_label(self, int idm):
        """Return the label of a mineral."""
        self.mineral_kinetic_check_bounds(idm)
        return bytes(self._get().get_label_mineral_kinetic(idm)).decode()

    def mineral_kinetic_label_to_id(self, mineral):
        """ Return the id of a mineral governed by kinetics"""
        return self.mineral_kinetic_id(mineral)


    def print_minerals_kinetic(self):
        """ Print the solid phases governed by kinetic"""
        print("The solid phases governed by kinetics law are :")
        cdef int i
        for i in range(self.nb_mineral_kinetic):
            print(" - "+str(i)+" : "+self.mineral_kinetic_id_to_label(i))

    # properties
    # ----------

    def logk_mineral_kinetic(self, mineral):
        """Return the equilibrium constant of a solid phase"""
        return self._get().logk_mineral_kinetic(self.mineral_kinetic_id(mineral))

    def nu_mineral_kinetic(self, mineral, comp):
        """Return stoichometric coefficient of a mineral"""
        return self._get().nu_mineral_kinetic(
                self.mineral_kinetic_id(mineral),
                self.component_id(comp)
                )

    def molar_mass_mineral_kinetic(self, mineral, UnitsBox units_box=None):
        """Return the molar mass (kg/mol) of mineral 'idm'"""
        idm = self.mineral_kinetic_id(mineral)
        if units_box is None:
            return self._get().molar_mass_mineral_kinetic(idm)
        else:
            return self._get().molar_mass_mineral_kinetic(idm, deref(units_box._get()))

    def molar_volume_mineral_kinetic(self, mineral, UnitsBox units_box=None):
        """Return the molar volume (m^3/mol) of mineral 'idm'"""
        idm = self.mineral_kinetic_id(mineral)
        if units_box is None:
            return self._get().molar_volume_mineral_kinetic(idm)
        else:
            return self._get().molar_volume_mineral_kinetic(idm, deref(units_box._get()))

    def density_mineral_kinetic(self, mineral, UnitsBox units_box=None):
        """Return the density (kg/m^3) of a mineral 'idm'"""
        idm =  self.mineral_kinetic_id(mineral)
        if units_box is None:
            return (self._get().molar_mass_mineral_kinetic(idm)
                /   self._get().molar_volume_mineral_kinetic(idm)
                )
        else:
            return (self._get().molar_mass_mineral_kinetic(idm, deref(units_box._get()))
                /   self._get().molar_volume_mineral_kinetic(idm, deref(units_box._get()))
                )

    # ========
    #   Gas
    # ========

    property nb_gas:
        """The number of gas in the database"""
        def __get__(self):
            return self._get().nb_gas()

    def gas_check_bounds(self, int idg):
        """Check if idg is a correct index for a gas"""
        if (idg >= self.nb_gas or idg < 0):
            raise ValueError("'"+str(idg)+
                             "' is not a valid index for a gas")

    def gas_id(self, gas):
        """Return the id of 'gas'"""
        if self.is_index(gas):
            idg = gas
        else:
            idg = self._get().get_id_gas(ensure_bytes(gas))
        if (idg == no_species or idg >= self.nb_gas):
            raise ValueError("Species : '"+ensure_str(gas)+"' is not a valid gas.")
        return idg
    
    def gas_id_to_label(self, int idg):
        """Return the label of a gas."""
        self.gas_check_bounds(idg)
        return bytes(self._get().get_label_gas(idg)).decode()

    def gas_label_to_id(self, gas):
        """ Return the id of a mineral"""
        return self.gas_id(gas)
    
    def print_gas(self):
        """ Print the gas"""
        print("The gas are :")
        cdef int i
        for i in range(self.nb_gas):
            print(" - "+str(i)+" : "+self.gas_id_to_label(i))

    def remove_gas_phases(self):
        """Remove all the gas species in the database."""
        self.database.remove_gas_phases()

    def add_gas_phases(self, input_gas_phases):
        """Add some gas into the database.

        'input_gas_phases' is a JSON list of gas formatted as in the database.
        """
        self.database.add_gas_phases(ensure_bytes(input_gas_phases))

    def nu_gas(self, gas, comp):
        """Return the stoichiometric coefficient of 'idc' in 'idg' dissolution
        reaction."""
        return self._get().nu_gas(
                self.gas_id(gas),
                self.component_id(comp)
                )

    def logk_gas(self, gas):
        """Return the equilibrium constant of a gas"""
        return self._get().logk_mineral(self.gas_id(gas))


    def remove_half_cell_reactions(self):
        """Remove oxydoreduction model"""
        self.database.remove_half_cell_reactions()