#Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from libcpp.string cimport string

from specmicp.utils.safe_config cimport YAMLConfigHandle

cdef extern from "specmicp_common/physics/units.hpp" namespace "specmicp::units":
    cdef cppclass LengthUnit:
        pass

    cdef LengthUnit u_meter      "specmicp::units::LengthUnit::meter"
    cdef LengthUnit u_decimeter  "specmicp::units::LengthUnit::decimeter"
    cdef LengthUnit u_centimeter "specmicp::units::LengthUnit::centimeter"


    cdef cppclass MassUnit:
        pass

    cdef MassUnit u_kilogram  "specmicp::units::MassUnit::kilogram"
    cdef MassUnit u_gram      "specmicp::units::MassUnit::gram"


    cdef cppclass QuantityUnit:
        pass

    cdef QuantityUnit u_moles      "specmicp::units::QuantityUnit::moles"
    cdef QuantityUnit u_millimoles "specmicp::units::QuantityUnit::millimoles"

    cdef cppclass UnitsSet:
        UnitsSet()
        UnitsSet(const UnitsSet&)
        MassUnit mass
        LengthUnit length
        QuantityUnit quantity

    cdef UnitsSet SI_units
    cdef UnitsSet CMG_units


cdef extern from "specmicp_common/physics/io/units.hpp" namespace "specmicp::io":
    cdef string to_string(LengthUnit)
    cdef string to_string(MassUnit)
    cdef string to_string(QuantityUnit)

cdef extern from "specmicp_common/physics/io/configuration.hpp" namespace "specmicp::io":
    cdef UnitsSet cpp_configure_units "specmicp::io::configure_units" (YAMLConfigHandle&&) except +

# These are the function that allows us to overcome the "enum class" cython incompatibility
cdef extern from "set_units.hpp" namespace "specmicp::units":
    void set_meter(LengthUnit&)
    void set_decimeter(LengthUnit&)
    void set_centimeter(LengthUnit&)

cdef class UnitsBox:
    cdef UnitsSet* c_units
    cdef const UnitsSet* _get(self)
    cdef _set(self, const UnitsSet&)