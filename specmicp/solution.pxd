#Copyright (c) 2014,2015 Fabien Georget <fabieng@princeton.edu>, Princeton
#University #All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this
#list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice,
#this list of conditions and the following disclaimer in the documentation
#and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors
#may be used to endorse or promote products derived from this software without
#specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
#FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
#DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from libcpp cimport bool
from libcpp.string cimport string
from libcpp.vector cimport vector
from memory cimport shared_ptr
from specmicp.database cimport DataContainer, DatabaseManager
from specmicp.units cimport UnitsSet
from eigen cimport VectorXd

# The solution
# -------------
# note: access is restrictive since the user should use
# the extractor class to acces the data
cdef extern from "specmicp/adimensional/adimensional_system_solution.hpp" namespace "specmicp":
    cdef cppclass AdimensionalSystemSolution:
        AdimensionalSystemSolution()
        AdimensionalSystemSolution(const AdimensionalSystemSolution&)
        bool is_valid

# The solution extractor
# ----------------------
cdef extern from "specmicp/adimensional/adimensional_system_solution_extractor.hpp" namespace "specmicp":
    cdef cppclass AdimensionalSystemSolutionExtractor:
        AdimensionalSystemSolutionExtractor(
            const AdimensionalSystemSolution&,
            shared_ptr[DataContainer],
            UnitsSet
            ) except +

        double volume_fraction_water()
        double density_water()
        double saturation_water()
        double saturation_gas_phase()
        double mass_concentration_water()
        double porosity()
        double pE()
        double Eh()
        double molality_component(int)
        double activity_component(int)
        double volume_fraction_mineral(int)
        double mole_concentration_mineral(int)
        double mass_concentration_mineral(int)
        double saturation_index(int)
        double effective_saturation_index(int)
        double saturation_index_kinetic(int)
        double effective_saturation_index_kinetic(int)
        double free_surface_concentration(int)
        double ionic_strength()
        double pH()
        double molality_aqueous(int)
        double activity_aqueous(int)
        double fugacity_gas(int)
        double molality_sorbed_species(int)
        double total_concentration(int)
        VectorXd total_concentrations()
        double total_aqueous_concentration(int)
        double total_solid_concentration(int)
        double total_immobile_concentration(int)
        double volume_fraction_inert()
        double total_mass_concentration()
        double total_solid_mass_concentration()
        VectorXd get_main_variables()
        shared_ptr[DataContainer] get_database()

# The solution saver
# ------------------
cdef extern from "specmicp/io/adimensional_system_solution_saver.hpp" namespace "specmicp::io":
    cdef void save_solution_yaml(
         const string&,
         shared_ptr[DataContainer],
         const AdimensionalSystemSolution&,
         string db_path
         )

    cdef void save_solutions_yaml(
         const string&,
         shared_ptr[DataContainer],
         const vector[AdimensionalSystemSolution]&,
         string db_path
         )

# The solution reader
# ------------------
cdef extern from "specmicp/io/adimensional_system_solution_reader.hpp" namespace "specmicp::io":
    cdef AdimensionalSystemSolution parse_solution_yaml(
        string filepath,
        shared_ptr[DataContainer]&
        )

    cdef vector[AdimensionalSystemSolution] parse_solutions_yaml(
        string filepath,
        shared_ptr[DataContainer]&
       )

# Cython wrapper
# --------------
# the extractor only contains a reference to the solution
# so we need to store the solution also
cdef class SpecMiCPSolution:
    cdef AdimensionalSystemSolution* solution
    cdef AdimensionalSystemSolutionExtractor* extractor
    cdef DatabaseManager db

    cdef void set_solution(self,
        const AdimensionalSystemSolution& solution,
        shared_ptr[DataContainer] database,
        UnitsSet units_set
        )
    cdef AdimensionalSystemSolution* _get(self)
    cdef VectorXd get_main_variables(self)


