# README #

SpecMiCPPy is a python binding for [SpecMiCP][1]. It allows to set, solve and post-process speciation problems from python.

## Installing SpecMiCPPy ##

First, you  need a working installation of [SpecMiCP][1].

The configuration, build and installation of the package is currently managed by CMake.

### Configuration ###

To configure the package :

~~~~~~
mkdir build
cd build
cmake ../ -DSPECMICP_PYTHON_VERSION_3=off
make
~~~~~~

The build requires [SpecMiCP][1], [Eigen v3][2], and [Boost][3]. The path to the SpecMiCP libraries and include directory can be found using the following configuration flag :

~~~~~~
cmake ../specmicp_cython -DSPECMICP_PYTHON_VERSION_3=OFF -DSPECMICP_LIBS_DIR=<library directory> -DSPECMICP_INCLUDE_DIR=<include dir>
~~~~~~

### Install ###

To install the packet :

~~~~~~
make install
~~~~~~

The installation path can be changed by using the `-DCMAKE_INSTALL_PREFIX=<prefix>` during the configuration.

The `make install` command uses a Distutils script to perform the installation. Therefore it should be consistent with a 'normal' python installation.

## License ##

SpecMiCPPy is available under a 3-clause BSD license. The licence is available in the *COPYING* file.

## Tests ##

To run the tests use `make test`.

## Documentation ##

ToDo !


## More information ##

For more information about this project, see the [SpecMiCP repository][1].

[1]: https://bitbucket.org/specmicp/specmicp
[2]: http://eigen.tuxfamily.org/index.php?title=Main_Page
[3]: http://www.boost.org/