#! /usr/bin/env python3

import unittest as ut
import sys

lib_directory = "@python_module_path@"
database_path = b"@database_path@"

sys.path.insert(0, lib_directory)

import specmicp.database as database
import specmicp.utils.safe_config as spconf

conf_str =b"""
database:
    path: @database_path@
    list_solids_to_keep:
       - Portlandite
       - CSH,jennite
       - CSH,tobermorite
       - SiO2(am)
       - Calcite
       - Al(OH)3(mic)
       - C3AH6
       - C3ASO_41H5_18
       - C3ASO_84H4_32
       - C4AH19
       - C4AH13
       - C2AH7_5
       - CAH10
       - Monosulfoaluminate
       - Tricarboaluminate
       - Monocarboaluminate
       - Hemicarboaluminate
       - Gypsum
       - Ettringite
    remove_half_cells: true
    remove_sorbeds: true
    swap_components:
      - in: CO2
        out: HCO3[-]
      - in: Al(OH)4[-]
        out: Al[3+]
"""

class TestDatabase(ut.TestCase):

    def test_init(self):
        db = database.DatabaseManager(database_path)
        self.assertTrue(db.is_valid())
        self.assertTrue(db.nb_component > 0)
        self.assertTrue(db.nb_aqueous > 0)
        self.assertTrue(db.nb_mineral > 0)

    def test_access_component(self):
        db = database.DatabaseManager(database_path)
        label = db.component_id_to_label(2)
        self.assertTrue(db.component_id_to_label(2) == label)
        self.assertTrue(db.molar_mass_component(2) > 0)
        self.assertTrue(db.molar_mass_component(label) == db.molar_mass_component(2))

    def test_access_aqueous(self):
        db = database.DatabaseManager(database_path)
        self.assertTrue(db.nb_aqueous > 0)
        self.assertTrue(db.nb_aqueous > 2)
        self.assertTrue(db.nb_component > 3)
        label = db.aqueous_id_to_label(2)
        self.assertEqual(db.aqueous_label_to_id(label), 2)
        self.assertIsNotNone(db.nu_aqueous(2, 2))
        self.assertEqual(db.nu_aqueous(label, db.component_id_to_label(2)),
                         db.nu_aqueous(2, 2))
        self.assertIsNotNone(db.nu_aqueous(2, 3))
        self.assertEqual(db.nu_aqueous(label, db.component_id_to_label(3)),
                         db.nu_aqueous(2, 3))

    def test_access_mineral(self):
        db = database.DatabaseManager(database_path)
        self.assertTrue(db.nb_mineral > 0)
        self.assertTrue(db.nb_mineral > 2)
        self.assertTrue(db.nb_component > 3)
        label = db.mineral_id_to_label(2)
        self.assertEqual(db.mineral_label_to_id(label), 2)
        self.assertIsNotNone(db.nu_mineral(2, 2))
        self.assertEqual(db.nu_mineral(label, db.component_id_to_label(2)),
                         db.nu_mineral(2, 2))
        self.assertIsNotNone(db.nu_mineral(2, 3))
        self.assertEqual(db.nu_mineral(label, db.component_id_to_label(3)),
                         db.nu_mineral(2, 3))

    def test_switch_basis(self):
        db = database.DatabaseManager(database_path)
        swapping = {b"H[+]": b"HO[-]",
                    "Al[3+]": "Al(OH)4[-]"
                   }
        db.swap_components(swapping)
        for (key, value) in swapping.items():
            self.assertRaises(ValueError, db.component_label_to_id, key)
            self.assertNotEqual(db.aqueous_label_to_id(key), -1)
            self.assertNotEqual(db.component_label_to_id(value), -1)
            self.assertRaises(ValueError, db.aqueous_label_to_id, value)

    def test_mineral_keep_only(self):
        db = database.DatabaseManager(database_path)
        mineral_to_keep = ("Portlandite", "Gypsum", "Straetlingite")
        logks = [db.logk_mineral(mineral) for mineral in mineral_to_keep]
        db.minerals_keep_only(mineral_to_keep)
        self.assertEqual(db.nb_mineral, 3)
        for i in range(db.nb_mineral):
            self.assertTrue(db.mineral_id_to_label(i) in mineral_to_keep)
            self.assertTrue(db.logk_mineral(i) in logks)

    def test_remove_mineral(self):
        db = database.DatabaseManager(database_path)
        db.remove_solid_phases()
        self.assertEqual(db.nb_mineral, 0)
        self.assertEqual(db.nb_mineral_kinetic, 0)

    def test_remove_gas(self):
        db = database.DatabaseManager(database_path)
        db.remove_gas_phases()
        self.assertEqual(db.nb_gas, 0)

class TestDatabaseConf(ut.TestCase):
    def test_read_db_from_conf(self):
        conf = spconf.SafeConfigFile.read_from_string(conf_str)
        db = database.configure_database(conf, b"database")
        self.assertTrue(db.is_valid())


if __name__ == '__main__':
    ut.main()

