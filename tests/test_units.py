#! /usr/bin/env python

import unittest as ut
import sys

lib_directory = "@python_module_path@"

sys.path.insert(0, lib_directory)


import specmicp.units as units
import specmicp.utils.safe_config as spconf

conf_str = b"""
units:
    length: centimeter
    mass: gram
    quantity: millimoles
    
"""

class TestUnits(ut.TestCase):

    def test_units(self):
        si_units = units.get_SI_units()
        self.assertEqual(si_units.length  , b"meter")
        self.assertEqual(si_units.mass    , b"kilogram")
        self.assertEqual(si_units.quantity, b"moles")

        cmg_units = units.get_CMG_units()
        self.assertEqual(cmg_units.length  , b"centimeter")
        self.assertEqual(cmg_units.mass    , b"gram")
        self.assertEqual(cmg_units.quantity, b"moles")


        some_units = units.UnitsBox()
        some_units.length = "dm"
        self.assertEqual(some_units.length, b"decimeter")
        some_units.mass = "gram"
        self.assertEqual(some_units.mass, b"gram")
        some_units.quantity = "mmol"
        self.assertEqual(some_units.quantity, b"millimoles")


    def test_conf(self):
        conf_f = spconf.SafeConfigFile.read_from_string(conf_str)
        units_box = units.configure_units(conf_f, b"units")
        self.assertEqual(units_box.length  , b"centimeter")
        self.assertEqual(units_box.mass    , b"gram")
        self.assertEqual(units_box.quantity, b"millimoles")
        
        

if __name__ == '__main__':
    ut.main()
