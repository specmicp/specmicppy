#!/usr/bin/env python

import unittest as ut
import sys

lib_directory = "@python_module_path@"

sys.path.insert(0, lib_directory)

import specmicp.utils.safe_config as sc


cf_str=b"""
section1:
    section2:
         - key1: value1
         - key2: value2
    section3:
        - key3: [1, 2, 3]
        - key4: value3
"""

class TestConfig(ut.TestCase):

    def test_from_string(self):
        conf = sc.SafeConfigFile.read_from_string(cf_str)
        self.assertTrue(conf.is_valid())
        sec1 = conf.get_section(b"section1")
        self.assertTrue(sec1.is_valid())
        sec2 = sec1.get_section(b"section2")
        self.assertTrue(sec2.is_valid())

        sec2d = conf.get_section(b"section1/section2")
        self.assertTrue(sec2d.is_valid())


if __name__ == '__main__':
    ut.main()


