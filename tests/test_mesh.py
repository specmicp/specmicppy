# -*- coding: utf-8 -*-

import unittest as ut
import sys

lib_directory = "@python_module_path@"

sys.path.insert(0, lib_directory)

import specmicp.dfpm.mesh as mesh

class TestMesh(ut.TestCase):

    def test_uniform_mesh1d(self):
        amesh = mesh.get_uniform_mesh(dx=0.05, nb_nodes=10, section=5)

        self.assertEqual(amesh.nb_nodes(), 10)
        self.assertEqual(amesh.nb_elements(), 9)
        self.assertAlmostEqual(amesh.get_position(0), 0.0)
        self.assertAlmostEqual(amesh.get_position(1), 0.05)
        self.assertAlmostEqual(amesh.get_dx(1), 0.05)
        self.assertAlmostEqual(amesh.get_face_area(0), 5)
        self.assertAlmostEqual(amesh.get_volume_element(1), 0.05*5)
        self.assertAlmostEqual(amesh.get_volume_cell(1), 0.05*5)
        self.assertAlmostEqual(amesh.get_volume_cell_element(1, 1), 0.05*5/2.0)

        def func(id_node):
            return 1;

        self.assertAlmostEqual(amesh.integrate_function(func), 2*5*0.025 + 8*5*0.05)


if __name__ == '__main__':
    ut.main()
