#! /usr/bin/env python

import unittest as ut
import sys

lib_directory = "@python_module_path@"
database_path = b"@database_path@"

sys.path.insert(0, lib_directory)

import specmicp.database as database
#import specmicp.constraints as constraints
from specmicp.reactant_box import ReactantBox, configure_reactant_box
import specmicp.units as units
import specmicp.solver as solver
from specmicp.utils.safe_config import SafeConfigFile

from specmicp.logger import init_logger_cout_warning

conf_str = b"""
units:
    length: centimeter
    quantity: millimoles
database:
    path: @database_path@
    list_solids_to_keep:
       - Portlandite
       - CSH,jennite
       - CSH,tobermorite
       - SiO2(am)
       - Calcite
       - Al(OH)3(mic)
       - C3AH6
       - C3ASO_41H5_18
       - C3ASO_84H4_32
       - C4AH19
       - C4AH13
       - C2AH7_5
       - CAH10
       - Monosulfoaluminate
       - Tricarboaluminate
       - Monocarboaluminate
       - Hemicarboaluminate
       - Gypsum
       - Ettringite
    remove_half_cells: true
    remove_sorbeds: true
    swap_components:
      - in: Al(OH)4[-]
        out: Al[3+]
      - in: HO[-]
        out: H[+]
specmicp:
    constraints:
      fixed_saturation: 0.7
    formulation:
      aqueous:
      - amount: 473.0
        label: KOH
        unit: mmol/L
      - amount: 52.0
        label: NaOH
        unit: mmol/L
      solid_phases:
      - amount: 4.46
        label: Portlandite
        unit: mol/L
      - amount: 4.59
        label: CSH,jennite
        unit: mol/L
      - amount: 0.3
        label: Monosulfoaluminate
        unit: mol/L
      - amount: 0.6
        label: Al(OH)3(mic)
        unit: mol/L
      - amount: 0.1
        label: Calcite
        unit: mol/L
      solution:
        amount: 0.4
        unit: kg/L
"""


class TestSpecmicp(ut.TestCase):

    def setUp(self):
        init_logger_cout_warning()

        self.db = database.DatabaseManager(database_path)
        swapping = {
            b"H[+]": b"HO[-]",
            b"Si(OH)4": b"SiO(OH)3[-]",
            b"HCO3[-]": b"CO2"
            }
        self.db.swap_components(swapping)

    def test_solver(self):
        t_units = units.get_SI_units()

        rbox = ReactantBox(self.db, t_units)
        rbox.set_solution(500, b"L")
        rbox.add_solid_phase(b"C3S", 350, b"L")
        rbox.add_solid_phase(b"C2S", 50, b"L")
        rbox.add_aqueous_species(b"CO2", 0.1, b"mol/L")
        rbox.set_charge_keeper(b"HO[-]")

        constr = rbox.get_constraints(modify_database=True)

        the_solver = solver.SpecMiCPSolver(self.db, constr, None, units_box=t_units)
        the_solver.initialize_variables(0.5, -6)
        the_solver.solve(False)

        the_solution = the_solver.get_solution()
        self.assertTrue(abs(the_solution.pH() - 12.47409) < 1e-3)
        print(the_solution.main_variables())


    def test_solve_dm(self):
        t_units = units.get_SI_units()
        t_units.length = "decimeter"

        rbox = ReactantBox(self.db, t_units)
        rbox.set_solution(0.5, b"L")
        rbox.add_solid_phase(b"C3S", 0.35, b"L")
        rbox.add_solid_phase(b"C2S", 0.05, b"L")
        rbox.add_aqueous_species(b"CO2", 0.1, b"mol/L")
        rbox.set_charge_keeper(b"HO[-]")

        constr = rbox.get_constraints(modify_database=True)

        the_solver = solver.SpecMiCPSolver(self.db, constr, None, units_box=t_units)

        self.assertEqual(the_solver.get_units().length, b"decimeter")

        the_solver.initialize_variables(0.5, -6)
        the_solver.solve(False)

        the_solution = the_solver.get_solution()
        self.assertTrue(abs(the_solution.pH() - 12.47409) < 1e-3)


class TestConfigSpecmicp(ut.TestCase):
    def test_conf(self):
        conf_f = SafeConfigFile.read_from_string(conf_str)
        t_units = units.configure_units(conf_f, b"units")
        db = database.configure_database(conf_f, b"database")
        rbox = configure_reactant_box(db, t_units, conf_f, b"specmicp")
        
        constr = rbox.get_constraints(modify_database=True)

        the_solver = solver.SpecMiCPSolver(db, constr, None, units_box=t_units)
        # initialization to have solution 100% of the time instead of 99%
        the_solver.initialize_variables(0.7, {b"HO[-]": -2, b"Ca[2+]": -3})
        the_solver.set_maximum_iterations(500)
        the_solver.set_maximum_step_length(50, 100)
        return self.assertTrue(the_solver.solve(False))

        
if __name__ == '__main__':
    ut.main()
